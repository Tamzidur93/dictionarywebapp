webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ".header {\r\n  font-family: 'Lato';\r\n  margin: 70px;\r\n  margin-bottom: 35px;\r\n  font-size: 55px;\r\n}\r\n\r\n.content {\r\n  /*margin-left: 70px;\r\n  margin-right: 70px;*/\r\n  padding-left: 70px;\r\n  padding-right: 70px;\r\n  display: inline-block;\r\n  width: 100%;  \r\n}\r\n"

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<!--<div style=\"text-align:center\">\n  <h1>\n    Welcome to {{ title }}!\n  </h1>\n  <img width=\"300\" alt=\"Angular Logo\" src=\"data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNTAgMjUwIj4KICAgIDxwYXRoIGZpbGw9IiNERDAwMzEiIGQ9Ik0xMjUgMzBMMzEuOSA2My4ybDE0LjIgMTIzLjFMMTI1IDIzMGw3OC45LTQzLjcgMTQuMi0xMjMuMXoiIC8+CiAgICA8cGF0aCBmaWxsPSIjQzMwMDJGIiBkPSJNMTI1IDMwdjIyLjItLjFWMjMwbDc4LjktNDMuNyAxNC4yLTEyMy4xTDEyNSAzMHoiIC8+CiAgICA8cGF0aCAgZmlsbD0iI0ZGRkZGRiIgZD0iTTEyNSA1Mi4xTDY2LjggMTgyLjZoMjEuN2wxMS43LTI5LjJoNDkuNGwxMS43IDI5LjJIMTgzTDEyNSA1Mi4xem0xNyA4My4zaC0zNGwxNy00MC45IDE3IDQwLjl6IiAvPgogIDwvc3ZnPg==\">\n</div>\n<h2>Here are some links to help you start: </h2>\n<ul>\n  <li>\n    <h2><a target=\"_blank\" rel=\"noopener\" href=\"https://angular.io/tutorial\">Tour of Heroes</a></h2>\n  </li>\n  <li>\n    <h2><a target=\"_blank\" rel=\"noopener\" href=\"https://github.com/angular/angular-cli/wiki\">CLI Documentation</a></h2>\n  </li>\n  <li>\n    <h2><a target=\"_blank\" rel=\"noopener\" href=\"https://blog.angular.io/\">Angular blog</a></h2>\n  </li>\n</ul>-->\n\n<div>\n  <div class=\"header\">\n    {{ title }}\n  </div>\n\n  <div class=\"content\">\r\n    <app-autocomplete class='autoComplete' [elements]=\"elements\" (changeEvent)=\"onChangeEvent($event)\" (selectEvent)=\"onSelectEvent($event)\"></app-autocomplete>\r\n  </div>\n  \n</div>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_dictionary_service__ = __webpack_require__("./src/app/services/dictionary.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(dataService) {
        this.dataService = dataService;
        this.title = "Dictionary App";
        this.elements = [];
    }
    AppComponent.prototype.onChangeEvent = function (value) {
        var _this = this;
        this.dataService.GetPartialmatch(value).subscribe(function (elements) {
            if (elements != null) {
                _this.elements = elements;
            }
            else {
                _this.elements = [];
            }
        });
    };
    AppComponent.prototype.onSelectEvent = function (value) {
        var _this = this;
        this.dataService.GetCompleteMatch(value).subscribe(function (element) {
            console.log(element);
            if (element.word === null || element.definition === null) {
                _this.elements = [];
            }
            else {
                _this.elements = [element];
            }
        });
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_dictionary_service__["a" /* DictionaryService */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_autocomplete_autocomplete_component__ = __webpack_require__("./src/app/components/autocomplete/autocomplete.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_dictionary_service__ = __webpack_require__("./src/app/services/dictionary.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["E" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_5__components_autocomplete_autocomplete_component__["a" /* AutocompleteComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* HttpModule */]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_6__services_dictionary_service__["a" /* DictionaryService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/classes/dictionary-element.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DictionaryElement; });
var DictionaryElement = /** @class */ (function () {
    function DictionaryElement(word, definition) {
        this.word = word;
        this.definition = definition;
    }
    return DictionaryElement;
}());



/***/ }),

/***/ "./src/app/components/autocomplete/autocomplete.component.css":
/***/ (function(module, exports) {

module.exports = "* {\r\n  font-family: 'Lato';\r\n}\r\n\r\ninput {\r\n  width: 100%;\r\n  display: inline-block;\r\n  font-size: 22px;\r\n}\r\n\r\n.container {\r\n  padding: 0;\r\n  border: 0;\r\n  margin: 0;\r\n  margin-top: 24px;\r\n  display: inline;\r\n  width: 100%;\r\n}\r\n\r\n.dropdown {\r\n  padding: 0px;\r\n  margin: 0px;\r\n  border: 0px;\r\n  list-style: none;\r\n}\r\n\r\n.dropdown li {\r\n    width: 100%;\r\n    background: rgba(255, 255, 255, 0.06);\r\n    cursor: pointer;\r\n    -webkit-transition: background linear .10s;\r\n    transition: background linear .10s;\r\n  }\r\n\r\n.dropdown hr {\r\n    margin: 0px;\r\n  }\r\n\r\n.dropdown a {\r\n    text-decoration: none;\r\n    color: inherit;\r\n  }\r\n\r\n/*.dropdown li:hover {\r\n    background: rgba(111, 111, 111, 0.11);\r\n    transition: background linear .10s;\r\n  }*/\r\n\r\n.dropdown li.active {\r\n    background: rgba(111, 111, 111, 0.11);\r\n    -webkit-transition: background linear .10s;\r\n    transition: background linear .10s;\r\n  }\r\n\r\n.match {\r\n  margin-top: 16px;\r\n}\r\n\r\n.match .word {\r\n  font-size: 24px;\r\n  font-weight: bold;\r\n}\r\n\r\n.match .definition {\r\n  margin-top: 12px;\r\n}\r\n\r\n.not_found {\r\n  margin-top: 16px;\r\n}\r\n"

/***/ }),

/***/ "./src/app/components/autocomplete/autocomplete.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\r\n  <input\r\n         class=\"input\"\r\n         type=\"text\"\r\n         placeholder=\"Type something\"\r\n         (keyup.enter)=\"OnEnterPressed()\"\r\n         (keydown.arrowdown)=\"SelectNextIndex()\"\r\n         (keydown.arrowup)=\"SelectPrevIndex()\"\r\n         [(ngModel)]=\"inputValue\"\r\n         (ngModelChange)=\"OnValueChange()\" />\r\n\r\n  <div class=\"container\">\r\n    <ul class=\"dropdown\" *ngIf=\"elements.length > 1\">\r\n      <li\r\n          *ngFor=\"let elem of elements; let i = index\"\r\n          id=\"item-{{i}}\"\r\n          [class.active]=\"getActiveState(i)\"\r\n          (mouseenter)=\"setSelectionIndex(i)\"\r\n          (click)=\"OnValueSelected(elem.word)\"\r\n          >\r\n        <!-- do routing for href -->\r\n        <a ng-href=\"#\" >{{elem.word}}</a> \r\n        <hr />\r\n      </li>\r\n    </ul>   \r\n\r\n    <div class=\"match\" *ngIf=\"elements.length === 1\">\r\n      <span class=\"word\">{{elements[0].word}}</span>\r\n      <p class=\"definition\">\r\n        {{elements[0].definition}}\r\n      </p>\r\n    </div>\r\n\r\n    <div class=\"not_found\" *ngIf=\"elements.length===0\">\r\n      <!--<p *ngIf=\"(inputValue === '')\"> type something </p>-->\r\n      <p *ngIf=\"(inputValue !== '')\"> found nothing </p>\r\n    </div>\r\n\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/autocomplete/autocomplete.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutocompleteComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AutocompleteComponent = /** @class */ (function () {
    function AutocompleteComponent() {
        this.inputValue = "";
        this.selectionIndex = 0;
        this.changeEmitter = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* EventEmitter */]();
        this.selectEmitter = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* EventEmitter */]();
    }
    AutocompleteComponent.prototype.OnValueChange = function () {
        if (this.inputValue != "") {
            this.changeEmitter.emit(this.inputValue);
            this.selectionIndex = 0;
        }
    };
    AutocompleteComponent.prototype.OnValueSelected = function (selection) {
        if (selection != "" && this.elements.length != 1) {
            this.inputValue = selection;
            this.selectEmitter.emit(selection);
            this.selectionIndex = 0;
        }
    };
    AutocompleteComponent.prototype.OnEnterPressed = function () {
        if (this.elements.length > 1) {
            var selection = this.elements[this.selectionIndex].word;
            this.OnValueSelected(selection);
        }
    };
    AutocompleteComponent.prototype.SelectNextIndex = function () {
        if (this.elements.length > 1) {
            this.inputValue = this.elements[this.selectionIndex].word;
            this.selectionIndex++;
            if (this.selectionIndex > this.elements.length - 1) {
                this.selectionIndex--;
            }
            console.log(this.selectionIndex);
        }
    };
    AutocompleteComponent.prototype.SelectPrevIndex = function () {
        if (this.selectionIndex > 0 && this.elements.length > 1) {
            this.selectionIndex--;
            this.inputValue = this.elements[this.selectionIndex].word;
            console.log(this.selectionIndex);
        }
    };
    AutocompleteComponent.prototype.setSelectionIndex = function (index) {
        this.selectionIndex = index;
        console.log(index);
    };
    AutocompleteComponent.prototype.getActiveState = function (index) {
        return index === this.selectionIndex;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["z" /* Input */])(),
        __metadata("design:type", Array)
    ], AutocompleteComponent.prototype, "elements", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* Output */])("changeEvent"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* EventEmitter */])
    ], AutocompleteComponent.prototype, "changeEmitter", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* Output */])("selectEvent"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* EventEmitter */])
    ], AutocompleteComponent.prototype, "selectEmitter", void 0);
    AutocompleteComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'app-autocomplete',
            template: __webpack_require__("./src/app/components/autocomplete/autocomplete.component.html"),
            styles: [__webpack_require__("./src/app/components/autocomplete/autocomplete.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AutocompleteComponent);
    return AutocompleteComponent;
}());



/***/ }),

/***/ "./src/app/services/dictionary.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DictionaryService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_observable_throw__ = __webpack_require__("./node_modules/rxjs/_esm5/add/observable/throw.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__classes_dictionary_element__ = __webpack_require__("./src/app/classes/dictionary-element.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var DictionaryService = /** @class */ (function () {
    function DictionaryService(http) {
        this.ApiURL = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].apiUrl;
        this.http = http;
    }
    DictionaryService.prototype.GetPartialmatch = function (word) {
        //var res = this.http.get(this.ApiURL + "api/Dictionary/partial/" + word);
        //console.log(res);
        var res = this.http.get(this.ApiURL + "api/Dictionary/partial/" + word).map(function (response) {
            var elems = response.json();
            if (elems !== null) {
                return elems.map(function (elem) { return new __WEBPACK_IMPORTED_MODULE_7__classes_dictionary_element__["a" /* DictionaryElement */](elem.key, elem.value); });
            }
            else {
                return null;
            }
        }).catch(this.handleError);
        return res;
    };
    DictionaryService.prototype.GetCompleteMatch = function (word) {
        var res = this.http.get(this.ApiURL + "api/Dictionary/match/" + word).map(function (response) {
            var elem = response.json();
            if (elem !== null) {
                return new __WEBPACK_IMPORTED_MODULE_7__classes_dictionary_element__["a" /* DictionaryElement */](elem.key, elem.value);
            }
            else {
                return null;
            }
        }).catch(this.handleError);
        return res;
    };
    DictionaryService.prototype.handleError = function (error) {
        console.error('ApiService::handleError', error);
        return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["a" /* Observable */].throw(error);
    };
    DictionaryService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], DictionaryService);
    return DictionaryService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false,
    apiUrl: "http://localhost:49705/"
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map