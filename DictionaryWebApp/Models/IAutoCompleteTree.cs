using System.Collections.Generic;

namespace DictionaryWebApp.Models
{
    public interface IAutoCompleteTree
    {
        List<KeyValuePair<string, string>> PartialMatch(string word);
        KeyValuePair<string, string> GetMatch(string word);
        int GetCount();
        void Insert(string word, string definition);
        void InsertAll(List<KeyValuePair<string, string>> maps);
    }
}
