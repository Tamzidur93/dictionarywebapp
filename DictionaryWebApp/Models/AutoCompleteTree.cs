using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DictionaryWebApp.Models
{
    public class AutoCompleteTree : IAutoCompleteTree
    {
        private PrefixNode<string> _root;
        private int _maxChildren = 256; // for all ascii
        private int _lowerCaseAlphabetOffset = 0;
        private int _maxReturnCount = 10;
        private string _dictionaryString;
        private string _dictionaryUrl = "./Models/dictionary.json";

        private object mutex = 5;

        public AutoCompleteTree(int maxReturnCount)
        {
            _root = new PrefixNode<string>(key: '*', val: "base", maxChildren: _maxChildren);
            _maxReturnCount = maxReturnCount;

            _dictionaryString = File.ReadAllText(_dictionaryUrl);

            var json = (JObject)JsonConvert.DeserializeObject(_dictionaryString);

            var count = 0;

            foreach (var item in json)
            {
                Insert(item.Key.ToString().ToLower(), item.Value.ToString());
                count++;
            }

            Console.WriteLine("tree construction completed - count=" + count);
        }

        public void Insert(string word, string definition)
        {
            lock (mutex)
            {
                var baseNode = _root;
                foreach (int character in word.ToLower().ToCharArray())
                {
                    // pass by reference can edit already inserted values
                    try
                    {
                        var child = baseNode.GetChild(character - _lowerCaseAlphabetOffset);

                        if (child == null)
                        {
                            baseNode.Insert(new PrefixNode<string>(character - _lowerCaseAlphabetOffset, _maxChildren));
                        }

                        baseNode = baseNode.GetChild(character - _lowerCaseAlphabetOffset);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"word={word}, character={character}");
                        throw e;
                    }
                }

                baseNode.Value = definition;
            }
        }

        public void InsertAll(List<KeyValuePair<string, string>> maps)
        {
            foreach (var wordMap in maps)
            {
                Insert(word: wordMap.Key, definition: wordMap.Value);
            }
        }

        public KeyValuePair<string, string> GetMatch(string word)
        {
            if (word == null)
            {
                return new KeyValuePair<string, string>(null, null);
            }

            var baseNode = _root;
            foreach (int character in word.ToLower().ToCharArray())
            {
                if (baseNode == null) { break; }
                baseNode = baseNode.GetChild(character - _lowerCaseAlphabetOffset);
            }

            if (baseNode != null && baseNode.Value != null)
            {
                return new KeyValuePair<string, string>(word, baseNode.Value);
            }
            else
            {
                return new KeyValuePair<string, string>(null, null);
            }
        }

        public int GetCount()
        {
            var count = 0;

            PrefixNode<String> baseNode = _root;

            var nodes = new List<PrefixNode<string>>();
            nodes.Add(baseNode);

            while (nodes.Count > 0)
            {
                baseNode = nodes.First();

                if (baseNode.Value != null)
                {
                    count++;
                }

                foreach (var child in baseNode.Children)
                {
                    if (child != null)
                    {
                        nodes.Add(child);
                    }
                }
                nodes.Remove(baseNode);
            }

            return count;
        }

        public List<KeyValuePair<string, string>> PartialMatch(string word)
        {

            if (word == null)
            {
                return null;
            }

            var baseNode = _root;
            foreach (int character in word.ToLower().ToCharArray())
            {
                if (baseNode == null) { break; }
                baseNode = baseNode.GetChild(character - _lowerCaseAlphabetOffset);
            }

            if (baseNode == null)
            {
                return null;
            }

            var res = new List<KeyValuePair<string, string>>();

            //if (baseNode.Value == null)
            //{
            var nodes = new List<KeyValuePair<PrefixNode<string>, string>>();
            nodes.Add(new KeyValuePair<PrefixNode<string>, string>(baseNode, word));

            while (nodes.Count > 0)
            {
                var map = nodes.First();
                baseNode = map.Key;
                var construct = map.Value;

                if (baseNode.Value != null)
                {
                    res.Add(new KeyValuePair<string, string>(construct, baseNode.Value));
                }

                if (res.Count == _maxReturnCount)
                {
                    return res;
                }

                foreach (var childNode in baseNode.Children)
                {
                    if (childNode != null)
                    {
                        nodes.Add(new KeyValuePair<PrefixNode<string>, string>(childNode, construct + ((char)childNode.Key)));
                    }
                }
                nodes.Remove(map);
            }

            return res;
            //}
            //else
            //{
            //    res.Add(new KeyValuePair<string, string>(word, baseNode.Value));
            //    return res;
            //}
        }
    }

    public class PrefixNode<T>
    {
        private int _key;
        private T _value;
        private int _maxChildren;
        private PrefixNode<T>[] children;

        public PrefixNode(int key, T val, int maxChildren)
        {
            _key = key;
            _value = val;
            _maxChildren = maxChildren;
            children = new PrefixNode<T>[_maxChildren];
        }

        public PrefixNode(int key, int maxChildren)
        {
            _key = key;
            _maxChildren = maxChildren;
            children = new PrefixNode<T>[_maxChildren];
        }

        public void Insert(PrefixNode<T> node)
        {
            children[node._key] = node;
        }

        public PrefixNode<T> GetChild(int index)
        {
            return children[index];
        }

        public PrefixNode<T>[] Children
        {
            get { return children; }
        }

        public int Key
        {
            get { return _key; }
        }

        public T Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
