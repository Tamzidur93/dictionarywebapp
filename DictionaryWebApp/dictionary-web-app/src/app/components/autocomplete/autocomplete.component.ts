import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DictionaryElement } from '../../classes/dictionary-element';

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.css']
})
export class AutocompleteComponent {

  inputValue: string = "";
  selectionIndex: number = 0;
  previousRef: Element;

  @Input()
  elements: DictionaryElement[];

  @Output("changeEvent")
  changeEmitter: EventEmitter<string> = new EventEmitter<string>();

  @Output("selectEvent")
  selectEmitter: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  OnValueChange() {
    if (this.inputValue != "") {
      this.changeEmitter.emit(this.inputValue);
      this.selectionIndex = 0;
    }
  }

  OnValueSelected(selection: string) {
    if (selection != "" && this.elements.length != 1) {
      this.inputValue = selection;
      this.selectEmitter.emit(selection);
      this.selectionIndex = 0;
    }
  }

  OnEnterPressed() {
    if (this.elements.length > 1) {
      let selection: string = this.elements[this.selectionIndex].word;
      this.OnValueSelected(selection);
    }    
  }

  SelectNextIndex() {
    if (this.elements.length > 1) {
      this.inputValue = this.elements[this.selectionIndex].word;

      this.selectionIndex++;

      if (this.selectionIndex > this.elements.length - 1) {
        this.selectionIndex--;
      }

      console.log(this.selectionIndex);     
    }
  }

  SelectPrevIndex() {
    if (this.selectionIndex > 0 && this.elements.length > 1) {
      
      this.selectionIndex--;
      this.inputValue = this.elements[this.selectionIndex].word;
      console.log(this.selectionIndex);
    }
  }

  setSelectionIndex(index: number) {
    this.selectionIndex = index;
    console.log(index);
  }

  getActiveState(index: number) {
    return index === this.selectionIndex;
  }
}
