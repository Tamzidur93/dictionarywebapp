import { Component } from '@angular/core';
import { DictionaryElement } from './classes/dictionary-element';
import { DictionaryService } from './services/dictionary.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  title = "Dictionary App";

  elements = [
  ];

  constructor(private dataService: DictionaryService) {
  }

  onChangeEvent(value: string) {
    this.dataService.GetPartialmatch(value).subscribe((elements) => {
      if (elements != null) {
        this.elements = elements;
      } else {
        this.elements = [];
      }
    });
  }

  onSelectEvent(value: string) {
    this.dataService.GetCompleteMatch(value).subscribe((element) => {
      console.log(element);
      if (element.word === null || element.definition === null) { 
        this.elements = [];
      } else {
        this.elements = [element];
      }
    });
  }

}
