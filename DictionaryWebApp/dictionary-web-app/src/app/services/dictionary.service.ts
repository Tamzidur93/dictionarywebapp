import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import { environment } from "../../environments/environment";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw'
import { DictionaryElement } from '../classes/dictionary-element';

@Injectable()
export class DictionaryService {

  private http: Http;
  private ApiURL: string = environment.apiUrl;

  constructor(http: Http) { this.http = http; }

  public GetPartialmatch(word: string): Observable<DictionaryElement[]> {
    //var res = this.http.get(this.ApiURL + "api/Dictionary/partial/" + word);
    //console.log(res);

    let res = this.http.get(this.ApiURL + "api/Dictionary/partial/" + word).map(response => {
      const elems = response.json();
      if (elems !== null) {
        return elems.map((elem) => new DictionaryElement(elem.key, elem.value));
      } else {
        return null;
      }
      
    }).catch(this.handleError);
    
    return res;
  }

  public GetCompleteMatch(word: string): Observable<DictionaryElement> {

    let res = this.http.get(this.ApiURL + "api/Dictionary/match/" + word).map(response => {      
      const elem = response.json();

      if (elem !== null) {
        return new DictionaryElement(elem.key, elem.value);
      } else {
        return null;
      }
      
    }).catch(this.handleError);

    return res;
  }

  private handleError(error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }
}
