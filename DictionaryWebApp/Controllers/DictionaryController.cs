﻿using DictionaryWebApp.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DictionaryWebApp.Controllers
{
    [Route("api/[controller]")]
    public class DictionaryController : Controller
    {
        private IAutoCompleteTree autoCompleteTree;

        public DictionaryController(IAutoCompleteTree autoCompleteTree)
        {
            this.autoCompleteTree = autoCompleteTree;
        }

        [HttpGet("partial/{partial}")]
        public JsonResult GetPartial(string partial)
        {
            var res = autoCompleteTree.PartialMatch(partial);
            return Json(res);
        }

        [HttpGet("match/{match}")]
        public JsonResult GetMatch(string match)
        {
            var res = autoCompleteTree.GetMatch(match);
            return Json(res);
        }
    }
}
